var plus_users_button = document.getElementById('plus_users');
var minus_users_button = document.getElementById('minus_users');
var permission_form = document.getElementById('permission_form');
var users_with_permission = document.getElementById('id_users_with_permission');
var users_without_permission = document.getElementById('id_users_without_permission');
var cancel_button = document.getElementById('cancel_permission');

plus_users_button.disabled = true;
minus_users_button.disabled = true;

function move_right(select_left, select_right) {
    selected_len = select_left.selectedOptions.length;

    for (var i = 0; i < selected_len; i++) {
        var option = select_left.selectedOptions[0].cloneNode(true);
        option.selected = true;
        select_left.selectedOptions[0].remove();
        select_right.options.add(option);
    }
    window.setTimeout(function () {select_right.focus();}, 0)
}

plus_users_button.onmousedown = function() {
    var select_left = users_without_permission;
    var select_right = users_with_permission;

    move_right(select_left, select_right)
};

minus_users_button.onmousedown = function() {
    var select_left = users_with_permission;
    var select_right = users_without_permission;

    move_right(select_left, select_right);
};

permission_form.onsubmit = function() {
    for(var i = 0; i < users_with_permission.length; i++) {
        users_with_permission.options[i].selected = true;
    }

    for(var i = 0; i < users_without_permission.length; i++) {
        users_without_permission.options[i].selected = true;
    }
};

users_with_permission.onfocus = function() {
    minus_users_button.disabled = false;
    plus_users_button.disabled = true;
};

users_with_permission.onblur = function() {
    minus_users_button.disabled = true;
};

users_without_permission.onfocus = function() {
    minus_users_button.disabled = true;
    plus_users_button.disabled = false;
};

users_without_permission.onblur = function() {
    plus_users_button.disabled = true;
};


