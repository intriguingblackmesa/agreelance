class UserPermissions:
    def __init__(self, write=False, read=False, modify=False,
                 owner=False, view_task=False, upload=False):
        self.write = write
        self.read=read
        self.modify = modify
        self.owner = owner
        self.view_task = view_task
        self.upload = upload

        