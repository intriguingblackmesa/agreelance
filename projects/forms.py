from django import forms
from user.models import Profile
from .models import Project, TaskFile, TaskOffer, Delivery, ProjectCategory, Team, Task
from django.contrib.auth.models import User

class ProjectForm(forms.ModelForm):
    title = forms.CharField(max_length=200)
    description = forms.Textarea()
    category_id = forms.ModelChoiceField(queryset=ProjectCategory.objects.all())

    class Meta:
        model = Project
        fields = ('title', 'description', 'category_id')

class TaskFileForm(forms.ModelForm):
    file = forms.FileField()

    class Meta:
        model = TaskFile
        fields = ('file',)

class ProjectStatusForm(forms.ModelForm):

    class Meta:
        model = Project
        fields = ('status',)

class TaskOfferForm(forms.ModelForm):
    title = forms.CharField(max_length=200)
    description = forms.Textarea()
    price = forms.NumberInput()

    class Meta:
        model = TaskOffer
        fields = ('title', 'description', 'price',)

class TaskOfferResponseForm(forms.ModelForm):
    feedback = forms.Textarea()

    class Meta:
        model = TaskOffer
        fields = ('status', 'feedback')

class TaskDeliveryResponseForm(forms.ModelForm):
    feedback = forms.Textarea()

    class Meta:
        model = Delivery
        fields = ('status', 'feedback')


PERMISSION_CHOICES = (
    ('Read','Read'),
    ('Write', 'Write'),
    ('Modify','Modify'),
)

class TaskPermissionForm(forms.Form):
    users_without_permission = forms.ModelMultipleChoiceField(required=False, widget=forms.SelectMultiple, queryset=None)
    users_with_permission = forms.ModelMultipleChoiceField(required=False, widget=forms.SelectMultiple, queryset=None)

    def __init__(self, task_id, permission2, *args, **kwargs):
        super(TaskPermissionForm, self).__init__(*args, **kwargs)

        users_with_permission = Profile.objects.none()
        users_without_permission = Profile.objects.none()
        if kwargs.get('data'):
            users_with_permission_ids = kwargs['data'].getlist('users_with_permission')
            users_without_permission_ids = kwargs['data'].getlist('users_without_permission')

            if users_with_permission_ids:
                users_with_permission = Profile.objects.filter(pk__in=users_with_permission_ids)
            if users_without_permission_ids:
                users_without_permission = Profile.objects.filter(pk__in=users_without_permission_ids)

        else:
            if permission2 == 'Read':
                users_with_permission = Task.objects.get(pk=task_id).read.all()

            elif permission2 == 'Write':
                users_with_permission = Task.objects.get(pk=task_id).write.all()
            else:
                users_with_permission = Task.objects.get(pk=task_id).modify.all()

            users_without_permission = Profile.objects.all().difference(users_with_permission)

        self.fields['users_without_permission'].queryset = users_without_permission
        self.fields['users_with_permission'].queryset = users_with_permission

class DeliveryForm(forms.ModelForm):
    comment = forms.Textarea()
    file = forms.FileField()

    class Meta:
        model = Delivery
        fields = ('comment','file')

class TeamForm(forms.ModelForm):
    name = forms.CharField(max_length=50)

    class Meta:
        model = Team
        fields = ('name',)

class TeamAddForm(forms.ModelForm):
    members = forms.ModelMultipleChoiceField(queryset=Profile.objects.all(), label='Members with read')

    class Meta:
        model = Team
        fields = ('members', )
