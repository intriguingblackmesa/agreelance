from unittest import skip

from django.contrib.auth.models import User
from django.test import TestCase, RequestFactory
from projects.models import Project, ProjectCategory, Task, TaskOffer, Notification
from projects.views import project_view, get_user_task_permissions
from user.models import Profile
from django.test import Client

class ProjectTestCase(TestCase):
    def setUp(self):
        self.client = Client()
        self.factory = RequestFactory()
        # Users
        self.admin = User.objects.create_superuser('admin', '', 'qwerty123')
        self.harrypotter = User.objects.create_user('harrypotter', 'hp@deskjet.example.com', 'qwerty123')
        self.joe = User.objects.create_user('joe', 'jd@garden.example.com', 'qwerty123')

        # Profiles
        self.admin_profile = Profile.objects.get(user=self.admin)
        self.harrypotter_profile = Profile.objects.get(user=self.harrypotter)
        self.joe_profile = Profile.objects.get(user=self.joe)

        # Project categories
        self.cleaning = ProjectCategory.objects.create(name='Cleaning')
        self.painting = ProjectCategory.objects.create(name='Painting')
        self.gardening = ProjectCategory.objects.create(name='Gardening')

        # Projects
        self.fixing_project = Project.objects.create(user=self.admin_profile, title='Fixing the outside of my house',
                               description='Need painting and landscaping',
                               category=self.gardening,
                               status='o')
        self.fixing_project.participants.add(self.joe_profile)

        # Tasks
        self.painting_task = Task.objects.create(project=self.fixing_project,
                                            title='Painting and gardening',
                                            description='Paining and gardening',
                                            budget=450,
                                            status='ad')

        self.project_path = '/projects/' + str(self.fixing_project.pk) + '/'
        self.signup_path = '/user/signup/'
        self.permission_path = self.project_path + 'tasks/' + str(self.painting_task.pk) + '/permissions/Read'

        # Task Offer
        self.best_offer = TaskOffer.objects.create(task=self.painting_task,
                                              title='The best offer',
                                              description='I can paint for you and find someone to do the gardening',
                                              price=400,
                                              offerer=self.joe_profile
                                             )

        self.signup_data = {
            'username': 'MyUser1',
            'first_name': 'Normal',
            'last_name': 'Alsonormal',
            'categories': self.cleaning.pk,
            'company': 'MyCompany',
            'email': 'anemail@email.com',
            'email_confirmation': 'anemail@email.com',
            'password1': 'Password123!',
            'password2': 'Password123!',
            'phone_number': '12345678',
            'country': 'USA',
            'state': 'Maryland',
            'city': 'Baltimore',
            'postal_code': '12345',
            'street_address': '123 Franklin Ln.'
            }

    ##############################Task 2######################################################
    def test_get_project_view_owner(self):
        client = self.client.login(username='admin', password='qwerty123')
        response = self.client.get(self.project_path)

        self.assertTemplateUsed(response, 'projects/project_view.html')
        self.assertEqual(response.status_code, 200)
        self.assertTrue(response.context['status_form'])
        self.assertTrue(response.context['offer_response_form'])
        self.assertFalse(response.context.get('task_offer_form'))

    def test_get_project_view_not_owner(self):
        self.client.login(username='harrypotter', password='qwerty123')
        response = self.client.get('/projects/' + str(self.fixing_project.pk) + '/')

        self.assertTemplateUsed(response, 'projects/project_view.html')
        self.assertEqual(response.status_code, 200)
        self.assertFalse(response.context.get('status_form'))
        self.assertFalse(response.context.get('offer_response_form'))
        self.assertTrue(response.context['task_offer_form'])

    def test_post_project_view_offer_response_accepted(self):
        self.client.login(username='admin', password='qwerty123')

        data = {'offer_response': True,
                'taskofferid': self.best_offer.pk,
                'status': 'a',
                'feedback': 'You did it'
                }
        response = self.client.post(self.project_path, data)

        self.assertEqual(response.status_code, 200)
        self.assertTrue(self.best_offer.task.read.all().filter(pk=self.joe_profile.pk))
        self.assertTrue(self.best_offer.task.write.all().filter(pk=self.joe_profile.pk))
        self.assertEqual(TaskOffer.objects.all().filter(pk=self.best_offer.pk)[0].status, 'a')

    def test_post_project_view_offer_response_pending(self):
        self.client.login(username='admin', password='qwerty123')

        data = {'offer_response': True,
                'taskofferid': self.best_offer.pk,
                'status': 'p',
                'feedback': 'I do not know'
                }
        response = self.client.post(self.project_path, data)

        self.assertEqual(response.status_code, 200)
        self.assertFalse(self.best_offer.task.read.all().filter(pk=self.joe_profile.pk))
        self.assertFalse(self.best_offer.task.write.all().filter(pk=self.joe_profile.pk))
        self.assertEqual(TaskOffer.objects.all().filter(pk=self.best_offer.pk)[0].status, 'p')

    def test_post_project_view_offer_response_declined(self):
        self.client.login(username='admin', password='qwerty123')


        data = {'offer_response': True,
                'taskofferid': self.best_offer.pk,
                'status': 'd',
                'feedback': 'This is not good'
                }
        response = self.client.post(self.project_path, data)

        self.assertEqual(response.status_code, 200)
        self.assertFalse(self.best_offer.task.read.all().filter(pk=self.joe_profile.pk))
        self.assertFalse(self.best_offer.task.write.all().filter(pk=self.joe_profile.pk))
        self.assertEqual(TaskOffer.objects.all().filter(pk=self.best_offer.pk)[0].status, 'd')

    def test_post_project_view_status_change(self):
        self.client.login(username='admin', password='qwerty123')

        data = {'status_change': True,
                'status': 'i'}
        response = self.client.post(self.project_path, data)

        self.assertEqual(response.status_code, 200)
        self.assertEqual(Project.objects.all().filter(pk=self.fixing_project.pk)[0].status, 'i')

    def test_post_project_view_offer_submit(self):
        self.client.login(username='harrypotter', password='qwerty123')

        data = {'offer_submit': True,
                'taskvalue': self.painting_task.pk,
                'title': 'The worst offer',
                'description': 'This is my offer',
                'price': 100}
        response = self.client.post(self.project_path, data)

        self.assertEqual(response.status_code, 200)
        self.assertEqual(TaskOffer.objects.all().filter(title='The worst offer')[0].offerer.pk,
                         self.harrypotter_profile.pk)

    def test_get_user_task_permissions_owner(self):
        permissions = get_user_task_permissions(self.admin, self.painting_task)

        expected = {
            'write': True,
            'read': True,
            'modify': True,
            'owner': True,
            'upload': True,
        }

        self.assertEqual(permissions, expected)

    def test_get_user_task_permissions_accepted_offerer(self):
        self.best_offer.status = 'a'
        self.best_offer.save()

        permissions = get_user_task_permissions(self.joe, self.painting_task)

        expected = {
            'write': True,
            'read': True,
            'modify': True,
            'owner': False,
            'upload': True,
        }

        self.assertEqual(permissions, expected)

    def test_get_user_task_permissions_no(self):
        expected = {
            'write': False,
            'read': False,
            'modify': False,
            'owner': False,
            'view_task': False,
            'upload': False,
        }

        permissions = get_user_task_permissions(self.joe, self.painting_task)
        self.assertEqual(permissions, expected)

    ##############################Task 3######################################################
    ##############################Boundary tests######################################################
    ## Tests for project offers

    def test_price_normal_offer(self):
        self.client.login(username='harrypotter', password='qwerty123')

        data = {'offer_submit': True,
                'taskvalue': self.painting_task.pk,
                'title': 'The worst offer',
                'description': 'This is my offer',
                'price': 100}
        response = self.client.post(self.project_path, data)

        # Tests that offer should have been created with normal value
        self.assertEqual(TaskOffer.objects.all().filter(title='The worst offer')[0].price, 100)

    def test_price_max_under_boundary_offer(self):
        self.client.login(username='harrypotter', password='qwerty123')

        data = {'offer_submit': True,
                'taskvalue': self.painting_task.pk,
                'title': 'The worst offer',
                'description': 'This is my offer',
                'price': 2**63 - 2}
        response = self.client.post(self.project_path, data)

        self.assertEqual(TaskOffer.objects.all().filter(title='The worst offer')[0].price, 2**63 - 2)

    def test_price_max_boundary_offer(self):
        self.client.login(username='harrypotter', password='qwerty123')

        data = {'offer_submit': True,
                'taskvalue': self.painting_task.pk,
                'title': 'The worst offer',
                'description': 'This is my offer',
                'price': 2**63 - 1}
        response = self.client.post(self.project_path, data)

        # Tests that offer should have been created with max value
        self.assertEqual(TaskOffer.objects.all().filter(title='The worst offer')[0].price, 2**63 - 1)

    def test_price_over_max_boundary_offer(self):
        self.client.login(username='harrypotter', password='qwerty123')

        data = {'offer_submit': True,
                'taskvalue': self.painting_task.pk,
                'title': 'The worst offer',
                'description': 'This is my offer',
                'price': 2**63}


        # Tests that offer should not have been created with greater than max value
        self.assertRaises(OverflowError, self.client.post, self.project_path, data)

    def test_price_above_min_boundary_offer(self):
        self.client.login(username='harrypotter', password='qwerty123')

        data = {'offer_submit': True,
                'taskvalue': self.painting_task.pk,
                'title': 'The worst offer',
                'description': 'This is my offer',
                'price': 1}
        response = self.client.post(self.project_path, data)

        # Tests that offer should have been created with min value
        self.assertEqual(TaskOffer.objects.all().filter(title='The worst offer')[0].price, 1)

    def test_price_min_boundary_offer(self):
        self.client.login(username='harrypotter', password='qwerty123')

        data = {'offer_submit': True,
                'taskvalue': self.painting_task.pk,
                'title': 'The worst offer',
                'description': 'This is my offer',
                'price': 0}
        response = self.client.post(self.project_path, data)

        # Tests that offer should have been created with min value
        self.assertEqual(TaskOffer.objects.all().filter(title='The worst offer')[0].price, 0)

    @skip('Offers should not be able to have a price below 0')
    def test_price_under_min_boundary_offer(self):
        self.client.login(username='harrypotter', password='qwerty123')

        data = {'offer_submit': True,
                'taskvalue': self.painting_task.pk,
                'title': 'The worst offer',
                'description': 'This is my offer',
                'price': -1}

        response = self.client.post(self.project_path, data)
        # Tests that offer should have not been created with less than min value
        self.assertFalse(TaskOffer.objects.all().filter(title='The worst offer'))

    def test_title_normal_offer(self):
        self.client.login(username='harrypotter', password='qwerty123')

        data = {'offer_submit': True,
                'taskvalue': self.painting_task.pk,
                'title': 'The worst offer',
                'description': 'This is my offer',
                'price': 100}
        response = self.client.post(self.project_path, data)

        self.assertTrue(TaskOffer.objects.all().filter(title='The worst offer'))

    def test_title_over_min_offer(self):
        self.client.login(username='harrypotter', password='qwerty123')

        data = {'offer_submit': True,
                'taskvalue': self.painting_task.pk,
                'title': 'AB',
                'description': 'This is my offer',
                'price': 100}
        response = self.client.post(self.project_path, data)

        self.assertTrue(TaskOffer.objects.all().filter(title='AB'))

    def test_title_min_offer(self):
        self.client.login(username='harrypotter', password='qwerty123')

        data = {'offer_submit': True,
                'taskvalue': self.painting_task.pk,
                'title': 'A',
                'description': 'This is my offer',
                'price': 100}
        response = self.client.post(self.project_path, data)

        self.assertTrue(TaskOffer.objects.all().filter(title='A'))

    def test_title_under_min_offer(self):
        self.client.login(username='harrypotter', password='qwerty123')

        data = {'offer_submit': True,
                'taskvalue': self.painting_task.pk,
                'title': '',
                'description': 'This is my offer',
                'price': 100}
        response = self.client.post(self.project_path, data)

        # Tests that offer should have been created with min value
        self.assertFalse(TaskOffer.objects.all().filter(title=''))

    def test_title_under_max_offer(self):
        self.client.login(username='harrypotter', password='qwerty123')

        data = {'offer_submit': True,
                'taskvalue': self.painting_task.pk,
                'title': 'A' * 199,
                'description': 'This is my offer',
                'price': 100}
        response = self.client.post(self.project_path, data)

        # Tests that offer should have been created with min value
        self.assertTrue(TaskOffer.objects.all().filter(title='A' * 199))

    def test_title_max_offer(self):
        self.client.login(username='harrypotter', password='qwerty123')

        data = {'offer_submit': True,
                'taskvalue': self.painting_task.pk,
                'title': 'A' * 200,
                'description': 'This is my offer',
                'price': 100}
        response = self.client.post(self.project_path, data)

        # Tests that offer should have been created with min value
        self.assertTrue(TaskOffer.objects.all().filter(title='A' * 200))

    def test_title_over_max_offer(self):
        self.client.login(username='harrypotter', password='qwerty123')

        data = {'offer_submit': True,
                'taskvalue': self.painting_task.pk,
                'title': 'A' * 201,
                'description': 'This is my offer',
                'price': 100}
        response = self.client.post(self.project_path, data)

        # Tests that offer should have been created with min value
        self.assertFalse(TaskOffer.objects.all().filter(title='A' * 201))

    def test_description_normal_offer(self):
        self.client.login(username='harrypotter', password='qwerty123')

        data = {'offer_submit': True,
                'taskvalue': self.painting_task.pk,
                'title': 'The worst offer',
                'description': 'This is my offer',
                'price': 100}
        response = self.client.post(self.project_path, data)
        self.assertTrue(TaskOffer.objects.all().filter(description='This is my offer'))

    def test_description_over_min_offer(self):
        self.client.login(username='harrypotter', password='qwerty123')

        data = {'offer_submit': True,
                'taskvalue': self.painting_task.pk,
                'title': 'AB',
                'description': 'AB',
                'price': 100}
        response = self.client.post(self.project_path, data)

        # Tests that offer should have been created with min value
        self.assertTrue(TaskOffer.objects.all().filter(description='AB'))

    def test_description_min_offer(self):
        self.client.login(username='harrypotter', password='qwerty123')

        data = {'offer_submit': True,
                'taskvalue': self.painting_task.pk,
                'title': 'Whelp',
                'description': 'A',
                'price': 100}
        response = self.client.post(self.project_path, data)

        self.assertTrue(TaskOffer.objects.all().filter(description='A'))

    def test_description_under_min_offer(self):
        self.client.login(username='harrypotter', password='qwerty123')

        data = {'offer_submit': True,
                'taskvalue': self.painting_task.pk,
                'title': 'Whelp',
                'description': '',
                'price': 100}
        response = self.client.post(self.project_path, data)

        self.assertFalse(TaskOffer.objects.all().filter(description=''))

    def test_description_under_max_offer(self):
        self.client.login(username='harrypotter', password='qwerty123')

        data = {'offer_submit': True,
                'taskvalue': self.painting_task.pk,
                'title': 'Task title',
                'description': 'A' * 499,
                'price': 100}
        response = self.client.post(self.project_path, data)

        # Tests that offer should have been created with min value
        self.assertTrue(TaskOffer.objects.all().filter(description='A' * 499))

    def test_description_max_offer(self):
        self.client.login(username='harrypotter', password='qwerty123')

        data = {'offer_submit': True,
                'taskvalue': self.painting_task.pk,
                'title': 'Task title',
                'description': 'A'*500,
                'price': 100}
        response = self.client.post(self.project_path, data)

        # Tests that offer should have been created with min value
        self.assertTrue(TaskOffer.objects.all().filter(description='A' * 500))

    def test_description_over_max_offer(self):
        self.client.login(username='harrypotter', password='qwerty123')

        data = {'offer_submit': True,
                'taskvalue': self.painting_task.pk,
                'title': 'Task title',
                'description': 'A' * 501,
                'price': 100}
        response = self.client.post(self.project_path, data)

        # Tests that offer should have been created with min value
        self.assertFalse(TaskOffer.objects.all().filter(title='A' * 501))

    ############Sign Up Form###########################
    # I will only perform boundary value test on username
    # Otherwise, with robust boundary value testing, I would have about 91 tests.
    def test_normal_signup(self):

        response = self.client.post(self.signup_path, self.signup_data)

        self.assertTrue(User.objects.all().filter(username='MyUser1'))

    def test_username_over_min_signup(self):
        self.signup_data['username'] = 'Ky'
        response = self.client.post(self.signup_path, self.signup_data)

        self.assertTrue(User.objects.all().filter(username='Ky'))
    def test_username_min_signup(self):
        self.signup_data['username'] = 'K'
        response = self.client.post(self.signup_path, self.signup_data)

        self.assertTrue(User.objects.all().filter(username='K'))

    def test_username_under_min_signup(self):
        self.signup_data['username'] = ''
        response = self.client.post(self.signup_path, self.signup_data)

        self.assertFalse(User.objects.all().filter(username=''))

    def test_username_over_max_signup(self):
        self.signup_data['username'] = 'Y' * 151
        response = self.client.post(self.signup_path, self.signup_data)

        self.assertFalse(User.objects.all().filter(username='Y' * 151))

    def test_username_max_signup(self):
        self.signup_data['username'] = 'Y' * 150
        response = self.client.post(self.signup_path, self.signup_data)

        self.assertTrue(User.objects.all().filter(username='Y' * 150))

    def test_username_under_max_signup(self):
        self.signup_data['username'] = 'Y' * 149
        response = self.client.post(self.signup_path, self.signup_data)

        self.assertTrue(User.objects.all().filter(username='Y' * 149))

#######OUTPUT COVERAGE TESTS###################
    @skip('A bug is that the user should be granted modify access as well')
    def test_accept_offer_output(self):
        self.client.login(username='admin', password='qwerty123')

        data = {'offer_response': True,
                'taskofferid': self.best_offer.pk,
                'status': 'a',
                'feedback': 'You did it'
                }
        response = self.client.post(self.project_path, data)


        self.assertTrue(self.best_offer.task.read.all().filter(pk=self.joe_profile.pk))
        self.assertTrue(self.best_offer.task.write.all().filter(pk=self.joe_profile.pk))
        self.assertTrue(self.best_offer.task.modify.all().filter(pk=self.joe_profile.pk))
        self.assertEquals(TaskOffer.objects.all().filter(pk=self.best_offer.pk)[0].status, 'a')
        self.assertContains(response, 'Accepted offer for')

########2-way domain tests#########################
# Obviously there are more cities, countries, state, etc. in the world than this
# but this should suffice for the assignment.
#
# City
# ----
# Madison
# Trondheim
#
# Country
# ------
# USA
# Norway
#
# State
# --------
# Wisconsin
# Trøndelag
#
# Zip code
# -------------
# 53711
# 7050
#
# Address
# ------------
# 674 S Whitney Way
# Frode Rinnans veg 1

    def test_domain_signup1(self):
        self.signup_data['country']='USA'
        self.signup_data['state']='Wisconsin'
        self.signup_data['city']='Madison'
        self.signup_data['postal_code']='53177'
        self.signup_data['street_address']='674 S Whitney Way'

        response = self.client.post(self.signup_path, self.signup_data)
        self.assertTrue(User.objects.all().filter(username='MyUser1'))

    @skip('The country should be Norway, not USA')
    def test_domain_signup2(self):
        self.signup_data['country']='USA'
        self.signup_data['state']='Trøndelag'
        self.signup_data['city']='Trondheim'
        self.signup_data['postal_code']='7050'
        self.signup_data['street_address']='Frode Rinnans veg 1'

        response = self.client.post(self.signup_path, self.signup_data)
        self.assertFalse(User.objects.all().filter(username='MyUser1'))

    @skip('Postal code and address are Norwegian')
    def test_domain_signup3(self):
        self.signup_data['country']='USA'
        self.signup_data['state']='Wisconsin'
        self.signup_data['city']='Madison'
        self.signup_data['postal_code']='7050'
        self.signup_data['street_address']='Frode Rinnans veg 1'

        response = self.client.post(self.signup_path, self.signup_data)
        self.assertFalse(User.objects.all().filter(username='MyUser1'))

    @skip('Wisconsin is not in Norway, nor is the address')
    def test_domain_signup4(self):
        self.signup_data['country']='Norway'
        self.signup_data['state']='Wisconsin'
        self.signup_data['city']='Trondheim'
        self.signup_data['postal_code']='7050'
        self.signup_data['street_address']='674 S Whitney Way'

        response = self.client.post(self.signup_path, self.signup_data)
        self.assertFalse(User.objects.all().filter(username='MyUser1'))

    @skip('City and zip code are Madison, everything else is Norwegian')
    def test_domain_signup5(self):
        self.signup_data['country']='Norway'
        self.signup_data['state']='Trøndelag'
        self.signup_data['city']='Madison'
        self.signup_data['postal_code']='53711'
        self.signup_data['street_address']='Frode Rinnans veg 1'

        response = self.client.post(self.signup_path, self.signup_data)
        self.assertFalse(User.objects.all().filter(username='MyUser1'))

    @skip('Postal code and street address are not Norwegian')
    def test_domain_signup6(self):
        self.signup_data['country']='Norway'
        self.signup_data['state']='Trøndelag'
        self.signup_data['city']='Trondheim'
        self.signup_data['postal_code']='53711'
        self.signup_data['street_address']='674 S Whitney Way'

        response = self.client.post(self.signup_path, self.signup_data)
        self.assertFalse(User.objects.all().filter(username='MyUser1'))

################System and integration testing######################3
    def test_offer_response_notification(self):
        self.client.login(username='admin', password='qwerty123')

        data = {'offer_response': True,
                'taskofferid': self.best_offer.pk,
                'status': 'a',
                'feedback': 'You did it'
                }
        response = self.client.post(self.project_path, data)

        self.assertTrue(Notification.objects.all().filter(event_text='You have an accepted offer'))

    def test_offer_submit_notification(self):
        self.client.login(username='harrypotter', password='qwerty123')

        data = {'offer_submit': True,
                'taskvalue': self.painting_task.pk,
                'title': 'The worst offer',
                'description': 'This is my offer',
                'price': 100}
        response = self.client.post(self.project_path, data)

        self.assertTrue(Notification.objects.all().filter(event_text='You have a pending offer'))

    def test_task_permission_remove_multiple_users(self):
        self.client.login(username='joe', password='qwerty123')

        self.best_offer.status = 'a'
        self.best_offer.save()

        data = {'users_with_permission': self.harrypotter_profile.pk,
                'users_with_permission': self.joe_profile.pk,
                'users_with_permission': self.admin_profile.pk
                }
        response = self.client.post(self.permission_path, data)

        data = {'users_without_permission': self.harrypotter_profile.pk,
                'users_without_permission': self.joe_profile.pk,
                'users_with_permission': self.admin_profile.pk
                }
        response = self.client.post(self.permission_path, data)

        #self.assertEquals(response.status_code, 200)
        self.assertFalse(self.best_offer.task.read.all().filter(pk=self.joe_profile.pk))
        self.assertFalse(self.best_offer.task.read.all().filter(pk=self.harrypotter_profile.pk))