# Generated by Django 2.1.7 on 2020-03-15 18:15

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('user', '0001_initial'),
        ('projects', '0003_notification'),
    ]

    operations = [
        migrations.AddField(
            model_name='notification',
            name='project_title',
            field=models.CharField(default='DEFAULT TITLE', max_length=50),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='notification',
            name='recipient',
            field=models.ForeignKey(default=None, on_delete=django.db.models.deletion.CASCADE, to='user.Profile'),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='notification',
            name='event_text',
            field=models.CharField(max_length=50),
        ),
    ]
