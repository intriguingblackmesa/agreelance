from django.urls import path
from . import views

urlpatterns = [
    path('', views.home, name='home'),
]

handler500 = 'agreelance.views.handler500'