from django_tables2 import tables, columns

from projects.models import Notification


class NotificationsTable(tables.Table):
    event_hyperlink = columns.TemplateColumn('<a href="{{record.event_url}}">View</a>', verbose_name='')
    select = columns.CheckBoxColumn(empty_values=(), footer="", accessor='pk')

    class Meta:
        model = Notification
        fields = ('project_title', 'event_text', 'event_type', 'ownership', 'status', 'event_instance')
        attrs = {"class": "table table-hover"}
