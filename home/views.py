from django.shortcuts import render, redirect, render_to_response
from pprint import pprint

from django.views.generic import DetailView, UpdateView
from django_tables2 import RequestConfig

from projects.models import Project, Notification
from .tables import NotificationsTable
import datetime
from agreelance import settings


def home(request):
    if request.user.is_authenticated:
        user = request.user
        user_projects = Project.objects.filter(user = user.profile)
        customer_projects = list(Project.objects.filter(participants__id=user.id).order_by().distinct())
        for team in user.profile.teams.all():
            customer_projects.append(team.task.project)
        cd = {}
        for customer_project in customer_projects:
            cd[customer_project.id] = customer_project

        customer_projects = cd.values()
        given_offers_projects = Project.objects.filter(pk__in=get_given_offer_projects(user)).distinct()

        if request.method == "POST":
            pks = request.POST.getlist("select")
            print(request.POST)
            Notification.objects.filter(pk__in=pks).delete()

        table = NotificationsTable(Notification.objects.filter(recipient=request.user.profile), order_by='-event_instance')
        RequestConfig(request).configure(table)

        return render(
        request,
        'index.html',
        {
            'user_projects': user_projects,
            'customer_projects': customer_projects,
            'given_offers_projects': given_offers_projects,
            'table': table,
            'settings': settings
        })
    else:
        return redirect('projects')

def get_given_offer_projects(user):
    project_ids = set()

    for taskoffer in user.profile.taskoffer_set.all():
        project_ids.add(taskoffer.task.project.id)

    return project_ids


# https://stackoverflow.com/a/24725091
def handler500(request, exception, template_name="500.html"):
    response = render_to_response(template_name)
    response.status_code = 500
    return response
