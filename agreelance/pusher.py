import pusher

from agreelance.settings import PUSHER_APP_ID, PUSHER_KEY, PUSHER_SECRET, PUSHER_CLUSTER

Pusher = pusher.Pusher(
    app_id=str(PUSHER_APP_ID),
    key=PUSHER_KEY,
    secret=PUSHER_SECRET,
    cluster=PUSHER_CLUSTER,
    ssl=True
)


def create_message(event_text, event_url):
    message = {'event_text': event_text,
               'event_url': event_url}

    return message
